<?php

namespace  ElmhurstProjects\EasyWorkflow\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use ElmhurstProjects\EasyWorkflow\Managers\Testing\TestResultsManager;

class TestResultsController extends Controller
{
    /**
     * @var TestResultsManager
     */
    private TestResultsManager $test_result_manager;

    public function __construct()
    {
        $this->test_result_manager = new TestResultsManager();
    }

    /**
     * Returns the current manifest file content
     * @return JsonResponse
     */
    public function results(): JsonResponse
    {
        return response()->json(['success' => true, 'test_results' => $this->test_result_manager->getManifest()]);
    }

    /**
     * Returns the latest output for the tests for an issue
     * @param int $issue_number
     * @return JsonResponse
     */
    public function resultForIssueNumber(int $issue_number): JsonResponse
    {
        return response()->json(['success' => true, 'test_result' => $this->test_result_manager->getResultsForIssueNumber($issue_number)]);
    }

}
