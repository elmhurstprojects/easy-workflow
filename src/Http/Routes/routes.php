<?php

use ElmhurstProjects\EasyWorkflow\Http\Controllers\TestResultsController;
use Illuminate\Support\Facades\Route;

Route::get('/tests/results', [TestResultsController::class, 'results']);
Route::get('/tests/result/{issue_number}', [TestResultsController::class, 'resultForIssueNumber']);

