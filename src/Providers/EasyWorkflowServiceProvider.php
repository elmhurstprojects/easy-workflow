<?php

namespace ElmhurstProjects\EasyWorkflow\Providers;

use ElmhurstProjects\EasyWorkflow\Console\TestRecorder;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class EasyWorkflowServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/easy_workflow.php' => config_path('easy_workflow.php'),
        ], 'easy_workflow');

        $this->loadRoutesFrom(__DIR__.'/../Http/Routes/routes.php');

        if ($this->app->runningInConsole()) {
            $this->commands([
                TestRecorder::class
            ]);
        }

        parent::boot();
    }
}
