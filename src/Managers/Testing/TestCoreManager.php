<?php

namespace ElmhurstProjects\EasyWorkflow\Managers\Testing;

use Carbon\Carbon;
use ElmhurstProjects\Core\Managers\CoreManager;

class TestCoreManager extends CoreManager
{
    protected array $manifest = [];

    /**
     * Gets the manifest array
     * @return array
     */
    public function getManifest(): array
    {
        if(count($this->manifest) > 0) return $this->manifest;

        return $this->extractManifest(file_get_contents($this->getManifestFilePath()));
    }

    /**
     * Saves thr manifest array to the file
     * @param array $manifest
     */
    public function saveManifest(array $manifest): void
    {
        $this->manifest = $manifest;

        file_put_contents($this->getManifestFilePath(), serialize($manifest));
    }

    /**
     * Location of the manifest file
     * @return string
     */
    protected function getManifestFilePath(): string
    {
        return storage_path() . '/test_output/manifest.json';
    }

    /**
     * Extracts the array from the file
     * @param $manifest_file_content
     * @return array
     */
    protected function extractManifest($manifest_file_content): array
    {
        return unserialize($manifest_file_content, ['allowed_classes' => ['stdClass']]);
    }

    /**
     * Gets the issue number from the issue name
     * @param string $issue_name
     * @return int
     */
    protected function getIssueNumberFromIssueName(string $issue_name): int
    {
        return (int)filter_var($issue_name, FILTER_SANITIZE_NUMBER_INT);
    }
}
