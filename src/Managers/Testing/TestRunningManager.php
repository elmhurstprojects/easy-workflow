<?php

namespace ElmhurstProjects\EasyWorkflow\Managers\Testing;

use Carbon\Carbon;
use ElmhurstProjects\GitHubAppAPI\Managers\GitHubAppAPIManager;

class TestRunningManager extends TestCoreManager
{
    protected $github_api_manager;
    protected int $issue_number;
    protected string $issue_name;
    protected string $github_user;
    protected string $repository;

    public function __construct(string $issue_name)
    {
        $this->github_user = config('easy_workflow.github_user');

        $this->repository = config('easy_workflow.repository');

        $this->github_api_manager = new GitHubAppAPIManager();

        $this->issue_number = $this->getIssueNumberFromIssueName($issue_name);

        $this->issue_name = $issue_name;

        $this->createManifest();

        parent::__construct();
    }

    /**
     * Start the testing foe an issue, remove labels, add testing, create manifest
     */
    public function startTesting(): void
    {
        $ref = $this->github_api_manager->get('repos/' . $this->github_user . '/' . $this->repository . '/commits/' . rawurlencode('issue/' . $this->issue_name) . '/check-suites');

        $this->line('Issue Name: ' . $this->issue_name . ' Head SHA: ' . $ref->check_suites[0]->head_sha);

        $params = [
            'name' => 'PHPUnit Testing',
            'head_sha' => $ref->check_suites[0]->head_sha,
            'started_at' => Carbon::now()->toIso8601String()
        ];

        $check_run = $this->github_api_manager->post('repos/' . $this->github_user . '/' . $this->repository . '/check-runs', $params);

        $existing_labels = $this->github_api_manager->get('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels');

        $this->line('Check Run ID: ' . $check_run->id);

        if (!$this->hasLabelInIssue($existing_labels, 'testing')) {
            $this->github_api_manager->post('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels', ['testing']);
        }

        if ($this->hasLabelInIssue($existing_labels, 'cancelled')) {
            $this->github_api_manager->delete('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels/cancelled');
        }

        if ($this->hasLabelInIssue($existing_labels, 'passed')) {
            $this->github_api_manager->delete('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels/passed');
        }

        if ($this->hasLabelInIssue($existing_labels, 'failed')) {
            $this->github_api_manager->delete('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels/failed');
        }

        $manifest = $this->getManifest();

        $manifest[$this->issue_number] = (object)[
            'issue_number' => $this->issue_number,
            'head_sha' => $ref->check_suites[0]->head_sha,
            'check_run_id' => $check_run->id,
            'start_time' => Carbon::now()->toDateTimeString(),
            'end_time' => null,
            'success' => null,
            'tests' => null,
            'assertions' => null,
            'failures' => null,
            'test_type' => 'phpunit'
        ];

        $this->saveManifest($manifest);

        $this->line('Update manifest with start time.');
    }

    public function endTesting(): void
    {
        $manifest = $this->getManifest();

        $existing_labels = $this->github_api_manager->get('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels');

        if ($this->hasLabelInIssue($existing_labels, 'testing')) {
            $this->github_api_manager->delete('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels/testing');
        }

        try {
            $test_results = $this->getResultsFromOutputFile();

            if ($test_results->success) {
                $this->github_api_manager->post('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels', ['passed']);
            } else {
                $this->github_api_manager->post('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels', ['failed']);
            }

            $manifest[$this->issue_number]->end_time = Carbon::now()->toDateTimeString();

            $manifest[$this->issue_number]->success = $test_results->success;

            $manifest[$this->issue_number]->tests = $test_results->tests;

            $manifest[$this->issue_number]->assertions = $test_results->assertions;

            $manifest[$this->issue_number]->failures = $test_results->failures;

            if ($test_results->success === true) {
                $params = [
                    'conclusion' => 'success',
                    'completed_at' => Carbon::now()->toIso8601String()
                ];
            } else {
                $params = [
                    'conclusion' => 'failure',
                    'completed_at' => Carbon::now()->toIso8601String()
                ];
            }
        } catch (\Exception $e) {
            $this->github_api_manager->post('repos/' . $this->github_user . '/' . $this->repository . '/issues/' . $this->issue_number . '/labels', ['cancelled']);

            $params = [
                'conclusion' => 'cancelled',
                'completed_at' => Carbon::now()->toIso8601String()
            ];
        }

        $this->github_api_manager->patch('repos/' . $this->github_user . '/' . $this->repository . '/check-runs/' . $manifest[$this->issue_number]->check_run_id, $params);

        $this->line('Update manifest with end time at ' . $manifest[$this->issue_number]->end_time);
        $this->line('Success: ' . $manifest[$this->issue_number]->success);
        $this->line('Tests: ' . $manifest[$this->issue_number]->tests);
        $this->line('Assertions: ' . $manifest[$this->issue_number]->assertions);
        $this->line('Failures: ' . $manifest[$this->issue_number]->failures);

        $this->saveManifest($manifest);
    }

    /**
     * Creates the manifest file if not already created
     */
    protected function createManifest(): void
    {
        if (!file_exists($this->getManifestFilePath())) {
            $file = fopen($this->getManifestFilePath(), 'wb');

            fwrite($file, serialize([]));

            fclose($file);
        }
    }

    /**
     * Checks the provide array of existing labels has the searching label
     *
     * @param array $existing_labels
     * @param string $searching_label
     *
     * @return bool
     */
    protected function hasLabelInIssue(array $existing_labels, string $searching_label): bool
    {
        $has_label = array_filter(
            $existing_labels,
            static function ($existing_label) use ($searching_label) {
                return $existing_label->name === $searching_label;
            }
        );

        return $has_label !== [];
    }

    /**
     * Get the results from the output file
     * @return \stdClass
     */
    protected function getResultsFromOutputFile(): \stdClass
    {
        $lines = file(storage_path() . '/test_output/' . $this->issue_name . '.txt');

        $last_line = (string)$lines[count($lines) - 1];

        $exploded = explode(',', $last_line);

        return (object)[
            'success' => strpos($last_line, 'OK') > -1,
            'tests' => filter_var($exploded[0], FILTER_SANITIZE_NUMBER_INT),
            'assertions' => filter_var($exploded[1], FILTER_SANITIZE_NUMBER_INT),
            'failures' => (isset($exploded[2])) ? filter_var($exploded[2], FILTER_SANITIZE_NUMBER_INT) : 0,
        ];
    }
}
