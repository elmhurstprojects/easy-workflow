<?php

namespace ElmhurstProjects\EasyWorkflow\Managers\Testing;

use Carbon\Carbon;

class TestResultsManager extends TestCoreManager
{
    /**
     * Gets the result for one test
     * @param int $issue_number
     * @return mixed|null
     */
    public function getResultsForIssueNumber(int $issue_number)
    {
        $manifest = $this->getManifest();

        foreach ($manifest as $test_result){
            if((int)$test_result->issue_number === $issue_number) return $test_result;
        }

        return null;
    }
}
