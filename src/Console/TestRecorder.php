<?php

namespace  ElmhurstProjects\EasyWorkflow\Console;

use Illuminate\Console\Command;
use  ElmhurstProjects\EasyWorkflow\Managers\Testing\TestRunningManager;

class TestRecorder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'easy:testRecorder {issue_name} {--start} {--end}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Records the Testing';

    protected TestRunningManager $testing_manager;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->testing_manager = new TestRunningManager($this->argument('issue_name'));

        if ($this->option('start') !== false) {
            $this->testing_manager->startTesting();
        }

        if ($this->option('end') !== false) {
            $this->testing_manager->endTesting();
        }

        $this->info('Testing Recorder Complete');
    }
}
