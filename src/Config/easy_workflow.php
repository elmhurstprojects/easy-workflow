<?php

return [
    /**
     * Github user name
     */
    'github_user' => 'your-github-user',

    /**
     * This is repository that you will be running workflow on
     */
    'repository' => 'repo name',

    /**
     * The output for the test recorder
     */
    'test_output_directory' => storage_path() . '/test_output'
];
