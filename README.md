# Github App API

This is the start of my own CI/CD system so I can have complete control

##Install

In composer repositories

```
     {
          "type": "vcs",
          "url":  "https://samjoyce@bitbucket.org/elmhurstprojects/easy-workflow.git"
      }
```

```
composer require elmhurstprojects/easy-workflow
```

Add to config/app.php
``` 
\ElmhurstProjects\EasyWorkflow\Providers\EasyWorkflowServiceProvider::class
```

Publish and set your configs
``` 
php artisan vendor:publish --tag=easy_workflow
```
``` 
php artisan vendor:publish --tag=github_app
```


##Usage

Recording of tests and adding of labels to Github issues,
this relies on the issues being prefix issue/ but dont use that
on the name below. I then put this command in the GitHun actions
to record on push.
``` 
php artisan easy:testRecorder  issue-name --start
```

``` 
php artisan easy:testRecorder  issue-name --end
```

Available API results endpoints
``` 
/tests/results
```
``` 
/tests/results/{issue_number}
```